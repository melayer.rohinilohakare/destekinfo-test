import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  @Output() dataEvent = new EventEmitter<string>();


  constructor() { }

  ngOnInit() {
  }

  onSearchData(event: KeyboardEvent) {
    console.log(event.target['value']);
    this.dataEvent.emit(event.target['value']);
  }
}
