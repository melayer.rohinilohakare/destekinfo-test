import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-firstcomponent',
  templateUrl: './firstcomponent.component.html',
  styleUrls: ['./firstcomponent.component.css']
})
export class FirstcomponentComponent implements OnInit {
  @Input() string1: string
  constructor(
    //  private sharedService: SharedService
  ) { }

  ngOnInit() {
  }

}
