import { Component, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = "homeapp"


  str1 = 'In the following example, a component defines two output properties that create event emitters. When the title is clicked, the emitter emits an open or close event to toggle the current visibility state.'
  str2 = 'These Angular docs help you learn and use the Angular framework and development platform, from your first application to optimizing complex single-page apps for enterprises. Tutorials and guides include downloadable examples to accelerate your projects.'
  str3 = 'You use the Angular CLI to create projects, generate application and library code, and perform a variety of ongoing development tasks such as testing, bundling, and deployment.'


  onSearch(str: string) {
    console.log('search ==')
    if (this.str1.includes(str)) {
      console.log(`Search String is in first component`)
    }
    if (this.str2.includes(str)) {
      console.log(`Search String is in second component`)
    }
    if (this.str3.includes(str)) {
      console.log(`Search String is in third component`)
    }
  }
}
